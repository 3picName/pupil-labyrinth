# Aufbau
#### Schriftliche Zusammenfassung:
/documentation/pdf/mazegaze.pdf

#### Arbeitsaufteilung:
/documentation/pdf/arbeitsaufteilung.pdf

#### Unity Projekt zum Importieren:
/src/unity

#### Release Build
https://gitlab.com/3picName/pupil-labyrinth/tree/master/src/unity/App/release

#### Debug Build (mit Cursor und großem Licht)
https://gitlab.com/3picName/pupil-labyrinth/tree/master/src/unity/App/debug

#### Anleitung zum Starten des Spiels (bitte Abschnitt "Bekannte Fehler" beachten)
https://gitlab.com/3picName/pupil-labyrinth/wikis/anleitung

#### Pupil Settings
https://gitlab.com/3picName/pupil-labyrinth/tree/master/src/unity/Assets/StreamingAssets

#### Anleitung für Pupil Settings
https://gitlab.com/3picName/pupil-labyrinth/wikis/pupil-config

#### Kontakt
kevin.mueller194@gmail.com


